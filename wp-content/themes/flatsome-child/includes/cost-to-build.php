<?php

/**
 * Shortcode for cost to build info page
 */
function ag_cost_to_build_plan_info() {
	$content = '<div class="ctb-plan-info row">';
	$content .= '<div class="ctb-plan-info-img large-6 column">';
	$content .= get_the_post_thumbnail($_GET['plan_id'], 'shop_single');
	$content .= '<h3>'. get_the_title($_GET['plan_id']) .'</h3>';
	$content .= '<h4>Plan ID: '. $_GET['plan_id'] .'</h4>';
	$content .= '</div>';
	$content .= '<div class="ctb-plan-info-objects large-6 column">';
	ob_start();
	?>
		<table>
		    <?php

		    foreach (ag_get_field_objects_sorted($_GET['plan_id']) as $key => $field) {
		        if ($key == 'designer-plan-name') continue; # Don't show this field
		        $value = $field['value'];

		        // End loop when foundation item is hit
		        if ($key == 'foundation')
		        	break;

			# This special handling for dimension fields assumes proper sorting
			if (preg_match('/-feet$/', $key)) {
				$feet = "$value $field[append]";
				continue;
			}
			if (preg_match('/-additional-inches$/', $key)) {
				$field['label'] = substr($field['label'], 0, strpos($field['label'], ' additional inches'));
				if ($value) {
					$value = "$feet-$value";
				} else {
					$value = $feet;
					$field['append'] = '';
				}
			}

		        if ($value) {
		            echo "<tr><td>$field[label]</td><td>";
		            if (is_array($value)) {
		                echo "<ul>";
		                foreach ($value as $eachvalue) {
		                    echo "<li>" . htmlentities($eachvalue) . "</li>";
		                }
		                echo "</ul>";
		            } else {
		                echo htmlentities($value . ' ' . $field['append']);
		            }
		            echo "</td></tr>\n";
		        }
		    }
		    ?>
		</table>
	<?php
	$content .= ob_get_clean();
	$content .= '</div>';

	$content .= '</div>';

	return $content;
}
add_shortcode('ag_cost_to_build_plan_info', 'ag_cost_to_build_plan_info');

/**
 * Add Cost to build product feed
 */
function ag_add_cost_to_build_api() {
	add_feed('cost-to-build', 'ag_cost_to_build_xml');
}
add_action('init', 'ag_add_cost_to_build_api');

/**
 * Retrieve XML file for cost to build
 */
function ag_cost_to_build_xml() {
	get_template_part('xml', 'cost-to-build');
}

/**
 * Link to Cost To Build site
 */
add_shortcode('abhp_cost_to_build_buy_now_button', function () {
    return '<a class="basic_button large-4" href="https://startbuild.com/store/CostToBuild/Import_Min.aspx?Vendor=americangables.com&amp;PlanCode='.$_GET['plan_id'].'">Buy Now</a>';
});
