<?php

/**
 * Wocoommerce product tabs
 */
function ag_filter_product_tabs( $tabs ) {
	unset($tabs['vendor']); // Remove the seller tab

	// Add the plan images tab
	$tabs['plan_images'] = array(
		'title' => __( 'Plans', 'woocommerce' ),
		'priority' => 12,
		'callback' => 'ag_product_plan_images_tab_content'
	);
	
	$tabs['additional_information'] = array(
		'title' => __( 'Additional Information', 'woocommerce' ),
		'priority' => 50,
		'callback' => 'ag_product_additional_information_tab'
	);
	
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'ag_filter_product_tabs' );

/**
 * Plans tab content
 */
function ag_product_plan_images_tab_content() {
	global $product;
	// The new tab content
	$attachment_ids = $product->get_gallery_attachment_ids();

	if ( $attachment_ids ) {
	        foreach ( $attachment_ids as $attachment_id ) {
			$title = get_the_title( $attachment_id );
			if (strpos($title, 'Floor Plan #') === 0) {
                list($src, $width, $height) = wp_get_attachment_image_src($attachment_id, 'full');
                echo "<a href=\"$src\">";
                echo wp_get_attachment_image($attachment_id, 'full');
                echo '</a>';
			}
	          }
	}
}

/**
 * Additional information tab content
 */
function ag_product_additional_information_tab() {
	ob_start();
	global $woocommerce, $post, $product;

	?>
	<table>
	    <?php

	    foreach (ag_get_field_objects_sorted() as $key => $field) {
	        if ($key == 'designer-plan-name') continue; # Don't show this field
	        $value = $field['value'];

		# This special handling for dimension fields assumes proper sorting
		if (preg_match('/-feet$/', $key)) {
			$feet = "$value $field[append]";
			continue;
		}
		if (preg_match('/-additional-inches$/', $key)) {
			$field['label'] = substr($field['label'], 0, strpos($field['label'], ' additional inches'));
			if ($value) {
				$value = "$feet-$value";
			} else {
				$value = $feet;
				$field['append'] = '';
			}
		}

	        if ($value) {
	            echo "<tr><td>$field[label]</td><td>";
	            if (is_array($value)) {
	                echo "<ul>";
	                foreach ($value as $eachvalue) {
	                    echo "<li>" . htmlentities($eachvalue) . "</li>";
	                }
	                echo "</ul>";
	            } else {
	                echo htmlentities($value . ' ' . $field['append']);
	            }
	            echo "</td></tr>\n";
	        }
	    }
	    ?>
	</table>

	<div id="whats_included">
		<p>What's included in these plans?</p>
		<ul>
			<li>The first sheet will typically be the front elevation at 1/4" scale with rafter framing details and misc exterior details</li>
			<li>The second sheet will typically be the sides and rear elevation with roof plan, all at 1/8" scale. Sometimes Sheet A1.1 and Sheet A1.2 will be combined</li>
			<li>The third sheet will be the foundation page at 1/4" scale. This will be a daylight basement foundation with most of our home plans (although some of our plans will come with a slab or crawl foundation)</li>
			<li>The fourth sheet will be the 1st floor plan at 1/4" scale with general notes</li>
			<li>The fifth sheet will be the 2nd floor plan at 1/4" scale. With a ranch plan, this sheet will be eliminated</li>
			<li>The sixth sheet will include the electrical layout at 3/16" scale and Kitchen and Bath elevations at 1/4" scale. This sheet will sometimes include misc interior elevations and details</li>
			<li>The last sheet will include misc details including typical wall sections, stair sections, etc.</li>
		</ul>
		<a href="<?php echo get_permalink(135165); ?>" target="_blank">Click Here to See Sample Plans</a>
	</div>

	<div id="package_choices_popup" class="white-popup wide mfp-hide">
	<h2>Package Choices</h2>
	<table class="shop_attributes">
	<tbody>
	<?php

	$packages = wc_get_product_terms( $product->id, 'pa_package', array('fields' => 'all', 'orderby' => 'menu_order'));

	foreach ($packages as $package) {
		?><tr>
		<th><?php echo $package->name; ?></th>
		<td><p><?php echo $package->description; ?></p></td>
		</tr>
		<?php
	}

	?>
	</tbody>
	</table>
	</div>
	<?php
	$output = ob_get_clean();

	echo $output;
}

/**
 * Display price next to variation name
 */
function ag_display_price_in_variation_option_name( $name ) {
	global $product;

	if (!$product) return $name;

	$term = get_term_by('name', $name, 'pa_package');
	$variations = $product->get_available_variations();

	foreach($variations as $subpost => $attrs) {
		if ($attrs['attributes']['attribute_pa_package'] == $term->slug) {
			$subprod = new WC_Product($attrs['variation_id']);
			$price = $subprod->get_price_html();
		}
	}

	if ( $price ) {
		return $name."&nbsp; $price";
	}
	return $name;
}
add_filter( 'woocommerce_variation_option_name', 'ag_display_price_in_variation_option_name' );

/**
 * Disable SKUs on frontend
 */
if( !is_admin() ) { 
	add_filter( 'wc_product_sku_enabled', '__return_false' ); 
}

/**
 * Print page button
 */
function ag_product_print_page_button() {
	global $wp_query, $product;
	$product_brochure = get_field('product_brochure');
	// print_r($product_brochure);exit;
	if ($product_brochure): ?>
		<div class="print_page_button basic_button">
			<a href="javascript: w=window.open('<?php echo $product_brochure; ?>'); w.print();">Print Brouchure</a>
		</div>
	<?php endif ?>
	<?php
}
add_action('woocommerce_product_meta_end', 'ag_product_print_page_button');

/**
 * Modify plan button
 */
function ag_product_modify_plan_button() {
	global $post;
	?>
	<a class="basic_button" href="<?= get_site_url(); ?>/?page_id=2115&plan_id=<?= $post->post_title ?>">
	Modify This Plan
	</a>
	<?php
}
add_action('woocommerce_product_meta_end', 'ag_product_modify_plan_button');


/**
 * Cost to build button
 */
function ag_product_cost_to_build_button() {
	global $post;
	?>
	<a class="basic_button" href="<?php echo get_permalink(1094); ?>?plan_id=<?= $post->ID ?>">
	Cost to Build
	</a>
	<?php
}
add_action('woocommerce_product_meta_end', 'ag_product_cost_to_build_button');

/**
 * Add reference boxes for single products beneath product image
 */
function ag_add_product_quick_references() {
	ob_start(); 
	?>
	<div id="quick-references">
	    <?php

	    $quick_reference_boxes = array(
	        'square-footage' => 'icon_path',
	        'bedrooms' => '',
	        'bathrooms' => '',
	        'width-feet' => '',
	        'width-additional-inches' => '',
	        'depth-feet' => '',
	        'depth-additional-inches' => '',
	        'garages' => '',
	    );

	    $reference = array_intersect_key(ag_get_field_objects_sorted(), $quick_reference_boxes);

	    foreach ($reference as $key => $field) {
	        $value = $field['value'];
	        # This special handling for dimension fields assumes proper sorting
	        if (preg_match('/-feet$/', $key)) {
	            $feet = "$value $field[append]";
	            continue;
	        }
	        if (preg_match('/^(.+)-additional-inches$/', $key, $matches)) {
	            $key = $matches[1];
	            $field['label'] = substr($field['label'], 0, strpos($field['label'], ' additional inches'));
	            if ($value) {
	                $value = "$feet-$value";
	            } else {
	                $value = $feet;
	                $field['append'] = '';
	            }
	        }

	        if ($value) {
	            ?>
	            <div class="quick-reference" style="background-image: url('<?php echo get_stylesheet_directory_uri() ; ?>/images/<?php echo $key; ?>.png');">
	                <div class="quick-reference-value"><?php echo htmlentities(str_replace('.', '', "$value $field[append]")); ?></div>
	                <div class="quick-reference-label"><?= htmlentities($field['label']) ?></div>
	            </div>
	        <?php
	        }
	    }
	    ?>

	</div><!-- #quick-references -->
	<?php
	$output = ob_get_clean();

	echo $output;
}
add_action('woocommerce_before_single_product_summary', 'ag_add_product_quick_references', 100);

/**
 * Add what's included link beneath product variations
 */
function ag_add_whats_included_to_product() {
	echo '<br><a href="#whats_included" id="whats_included_link">What&rsquo;s included in these plans?</a>';
}
add_action('woocommerce_before_add_to_cart_form', 'ag_add_whats_included_to_product', 0);

/**
 * Strip HTML from woocommerce variation options
 *
 * @param $value
 * @return string
 */
function ag_escape_woocommerce_variation_option_name($value) {
	return strip_tags($value);
}
add_filter('woocommerce_variation_option_name', 'ag_escape_woocommerce_variation_option_name');

/**
 * Move Foundation Options to beginning and make select input
 *
 * @param $addons
 * @return array
 */
function ag_add_foundation_options($addons, $id) {
	// Get product id
	global $real_id;
	if (empty($addons)) {
		$real_id = $id;
	}

	if (!empty($addons)) {

		foreach ($addons as $index => $addon) {
			if (strtolower($addon['name']) == 'foundation options') {
				foreach ($addon['options'] as $option) {
					if (strtolower($option['label']) == 'default') {
						$default_price = $option['price'];
					}
				}

				if ($default_price) {
					// Get included foundation types
					$foundation_included = get_post_meta($real_id, 'foundation')[0];
					$foundation_types = array (
						'Crawl Space',
						'Slab',
						'Unfinished Basement'
					);
					// Create options with price if foundation not included
					$foundation_options = array();
					foreach ($foundation_types as $type) {
						$price = $default_price;
						if (is_array($foundation_included) && in_array($type, $foundation_included)) {
							$price = '0';
						}
						$foundation_options[] = array (
							'label' => $type,
							'price' => $price,
							'min' => '',
							'max' => '',
						);
					}
					$addons[$index]['options'] = $foundation_options;
				}

			}
		}
	}
	return $addons;
}
add_filter('get_product_addons_fields', 'ag_add_foundation_options', 10, 2);

/**
 * Hide "Grand total" from addons
 *
 * @return false
 */
add_filter('woocommerce_product_addons_show_grand_total', '__return_false');

/**
 * Filter product loop for best selling category
 *
 * @param WP_Object $q
 * @return null
 */
function ag_modify_best_selling_category($q) {
	if ( is_product_category() && $q->query['product_cat'] == 'best-selling-house-plans' ) {
		$q->query_vars['product_cat'] = '';
		$q->query_vars['order'] = 'DESC';
		$q->query_vars['meta_key'] = 'total_sales';
	}
}
add_filter( 'woocommerce_product_query', 'ag_modify_best_selling_category' );

/**
 * Filters the options listed in the orderby dropdown
 *
 * @param array $sort_options
 *
 * @return array
 */
function ag_catalog_orderby( $sort_options ) {
	$sort_options = array();

	$sort_options['menu_order'] = 'Sort Results By';
	$sort_options['date']       = 'Newest';
	$sort_options['popularity'] = 'Popularity';
	$sort_options['sqft']       = 'Squarefoot Ascending';
	$sort_options['sqft-desc']  = 'Squarefoot Descending';

	return $sort_options;
}
add_filter( 'woocommerce_default_catalog_orderby_options', 'ag_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'ag_catalog_orderby' );

/**
 * Filters the query args based on the orderby dropdown, adds query args for our custom options.
 *
 * @param array $args
 *
 * @return array
 */
function ag_catalog_orderby_filter_args( $args ) {
	$orderby = ( isset( $_GET['orderby'] ) ? sanitize_text_field( $_GET['orderby'] )
		: apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) ) );

	switch ( $orderby ) {
		case 'sqft':
			$args['orderby']  = 'meta_value_num';
			$args['order']    = 'ASC';
			$args['meta_key'] = 'square-footage';
			break;
		case 'sqft-desc':
			$args['orderby']  = 'meta_value_num';
			$args['order']    = 'DESC';
			$args['meta_key'] = 'square-footage';
			break;
	}

	return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'ag_catalog_orderby_filter_args' );

/**
 * Move social sharing buttons
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 25 );

/**
 * Add question icon next to variable label
 */
function ag_add_info_link_to_variable($label) {
	if (is_product()) {
		$label = $label . '<a class="variation_question_mark question-icon">?</a>';
	}
	return $label;
}
add_filter( 'woocommerce_attribute_label', 'ag_add_info_link_to_variable');

function ag_add_header_to_form() {
	echo '<div class="form-header">Create your package</div>';
}
add_action('woocommerce_before_variations_form', 'ag_add_header_to_form');
add_action('woocommerce_before_add_to_cart_form', 'ag_add_header_to_form');

/**
 * Change Woocommerce price for dynamic pricing
 */
function ag_woocommerce_get_price( $price, $product )
{
    if( $price == 0 ) {
	    $addons = get_product_addons($product->id);
	    foreach ($addons as $addon) {
	    	if (strtolower($addon['name']) == 'package choices') {
	    		$options = array();
	    		foreach ($addon['options'] as $option) {
	    			$options[] = $option['price'];
	    		}
	    		$min = min($options);
	    		$max = max($options);
	    		$price = $min;
	    		// $price = $max;
	    	}
	    }
    }
    return $price;
}
add_filter( 'woocommerce_get_price', 'ag_woocommerce_get_price', 10, 2);

/**
 * Remove filters on description save
 */
function ag_remove_filter_on_addons($data, $i) {
	$addon_description  = $_POST['product_addon_description'];
	$data['description'] = stripslashes($addon_description[ $i ]);
	// wp_die(print_r($data));
	// wp_die(print_r($addon_description));
	return $data;
}
add_filter( 'woocommerce_product_addons_save_data', 'ag_remove_filter_on_addons', 10, 2 );
