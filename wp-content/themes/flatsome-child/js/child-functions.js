;
(function($, window, document, undefined) {
    var $win = $(window);
    var $doc = $(document);
    $doc.on('ready', function() {


        // if ($(wi)) {};
        $('.mobile-menu a').on('click', function() {
            $(this).toggleClass('active');
            $('.wide-nav').toggleClass('show');
            event.preventDefault();
        })
        // $('.header-nav > li > a').on('click', function(e) {
        //         _this = $(this);
        //         _this.toggleClass('tabbed');
        //         if (_this.hasClass('tabbed')) {
        //             e.preventDefault();
        //         }
        //     })
            // $('body').on('click', function() {
            // 	$('.header-nav li a').each(function() {
            // 		$(this).removeClass('tabbed');
            // 	})
            // })
            /*lightbox for large plan images*/
        $('#section-plan_images img').magnificPopup({
            type: 'image',
            tLoading: '<div class="loading dark"><i></i><i></i><i></i><i></i></div>',
            mainClass: 'my-mfp-zoom-in product-zoom-lightbox',
            removalDelay: 300,
            closeOnContentClick: true,
            gallery: {
                enabled: true,
                navigateByImgClick: false,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                verticalFit: false,
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });
        /* scroll to page internal links */
        $('a.page_internal_link').click(function(e) {
            var $this = $(this);
            var destinationAnchorName = ($this.attr('href') || '').substr(1);
            $.scrollTo('#' + destinationAnchorName + ', a[name="' + destinationAnchorName + '"]', 300, {
                offset: -150
            });
            e.preventDefault();
        });
        /*lightbox for package choices*/
        $('.variation_question_mark').magnificPopup({
            items: [{
                src: "#package_choices_popup",
                type: 'inline'
            }]
        });
        /*lightbox for addon description*/
        $('.addon-name').each(function() {
        	var link = $(this).find('.question-icon'),
        		title = link.data('mfp-title'),
        		content = $(this).next('.mfp-content-hidden').html();

        	link.magnificPopup({
        	    items: [{
        	        src: '<div class="addon-popup"><h2>' + title + '</h2>' + content + '</div>',
        	        type: 'inline'
        	    }]
        	});
        });

        /*lightbox for low price guarantee*/
        $('#low_price_button').magnificPopup({
            items: [{
                src: "#low_price_popup",
                type: 'inline'
            }]
        });
        /*lightbox for what's included in these plans*/
        $('#whats_included').clone().attr('id', 'whats_included_popup').attr('class', 'white-popup wide mfp-hide').insertAfter('#whats_included')
        $('#whats_included_link').magnificPopup({
            items: [{
                src: "#whats_included_popup",
                type: 'inline'
            }]
        });
        /*search form checkbox group roll up */
        $('.sidebar-inner form.searchandfilter').find('.sf-field-post-meta-foundation, .sf-field-post-meta-additional-rooms, .sf-field-post-meta-garage-type, .sf-field-post-meta-porch-type').each(function() {
            $(this).addClass('searchandfilter_abhp_rollup').children('h4').wrapInner('<a></a>').click(function() {
                $(this).next().is(":hidden") ? $(this).addClass('active').next().slideDown(200) : $(this).removeClass('active').next().slideUp(200);
            }).next().toggle();
        });
        /* validation for foundation options **/
        $(".variations_form").submit(function(event){
            var foundationOptions = $(this).find('.product-addon-foundation-options');
            if (foundationOptions.length) {
                var foundationOptionsValue = foundationOptions.find('select').val();
                if (!foundationOptionsValue) {
                    event.stopImmediatePropagation();
                    alert("Please select a foundation option.");
                    foundationOptions.find('.addon-name').css('color', 'red');
                    $('.single_add_to_cart_button').removeClass('loading');
                    return false;   
                }
            }
        });

    })
})(jQuery, window, document);