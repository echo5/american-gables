<?php
/**
 * Template Name: Cost To Build API
 */
header( 'Content-Type: application/rss+xml; charset=' . get_option( 'blog_charset' ), true );
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<?php
$_pf = new WC_Product_Factory();  
$_product = $_pf->get_product(intval($_GET['plan_id']));
$product_meta = get_post_meta($_product->id);
?>
<plan-data>
        <plan id="<?php echo $_product->id; ?>">
                <linktoplan><?php echo get_permalink($_product->id); ?></linktoplan>
                <headline><?php echo $_product->post->post_title; ?></headline>
                <vendorId><?php echo $_product->id; ?></vendorId>
                <planType>House</planType>
                <sqft><?php echo $product_meta['square-footage'][0]; ?></sqft>
                <bedrooms><?php echo $product_meta['bedrooms'][0]; ?></bedrooms>
                <baths><?php echo $product_meta['bathrooms'][0]; ?></baths>
                <halfbaths><?php echo $product_meta['half-baths'][0]; ?></halfbaths>
                <stories><?php echo $product_meta['floors'][0]; ?></stories>
                <width><?php echo $product_meta['width-feet'][0]; ?></width>
                <depth><?php echo $product_meta['depth-feet'][0]; ?></depth>
                <garagebays><?php echo $product_meta['garages'][0]; ?></garagebays>
                <foundation><?php echo implode(', ', unserialize($product_meta['foundation'][0])); ?></foundation>
                <designer id="1">American Gables</designer>
                <shortdescription><?php echo $_product->post->post_excerpt; ?></shortdescription>
                <longdescription><?php echo $_product->post->post_content; ?></longdescription>
                <amenities>
                        <?php if ($product_meta['bonus-room'][0]) { ?><amenity name="Bonus Room" sqft="<?php echo $product_meta['bonus-room'][0]; ?>" /><?php } ?>
                        <?php if ($product_meta['entry'][0]) { ?><amenity name="Entry" sqft="<?php echo $product_meta['entry'][0]; ?>" /><?php } ?>
                        <?php if ($product_meta['patio'][0]) { ?><amenity name="Patio" sqft="<?php echo $product_meta['patio'][0]; ?>" /><?php } ?>
                        <?php if ($product_meta['garage'][0]) { ?><amenity name="Garage" sqft="<?php echo $product_meta['garage'][0]; ?>" /><?php } ?>
                        <?php if ($product_meta['basement'][0]) { ?><amenity name="Basement" sqft="<?php echo $product_meta['basement'][0]; ?>" /><?php } ?>
                </amenities>
                <levels>
                        <?php if ($product_meta['first-floor'][0]) { ?><level name="Level One" sqft="<?php echo $product_meta['first-floor'][0]; ?>" /><?php } ?>
                        <?php if ($product_meta['second-floor'][0]) { ?><level name="Level Two" sqft="<?php echo $product_meta['second-floor'][0]; ?>" /><?php } ?>
                        <?php if ($product_meta['third-floor'][0]) { ?><level name="Level Three" sqft="<?php echo $product_meta['third-floor'][0]; ?>" /><?php } ?>
                </levels>
                <images>
                        <image type="Main" size="full" url="<?php echo wp_get_attachment_url(get_post_thumbnail_id($_product->id)); ?>" />
                        <?php $product_images = explode(',', $product_meta['_product_image_gallery'][0]); ?>
                        <?php foreach ($product_images as $image) { ?>
                        <image type="Gallery" size="full" url="<?php echo wp_get_attachment_url($image); ?>" />
                        <?php } ?>
                </images>
        </plan>
</plan-data>