module.exports = function( grunt ) {
    var paths = {
        // Destination file paths
        'dest': {
            'min': { // Minified file destinations
                'js': 'js/s8fl.min.js',
                'css': 'css/s8fl.min.css'
            },
            'js': 'js/s8fl.js',
            'scss': 'css/s8fl.css'
        },
        // Paths for watch tasks
        'watch': {
            'scss': [
                'src/scss/**/*.scss'
            ],
            'js': [
                'src/js/*.js'
            ]
        },
        // Non-vendor file paths
        'js': [
            'bower_components/magnific-popup/dist/jquery.magnific-popup.js',
            'src/js/header.js',
            'src/js/functions.js',
            'src/js/document-ready.jquery.js',
            'src/js/footer.js'
        ],
        'scss': [
            'src/scss/main.scss'
        ]
    };

    require( 'load-grunt-config' )(grunt, {
        init: true,
        data: {
            paths: paths
        },
        loadTasks: 'grunt/*.js'
    });
};
