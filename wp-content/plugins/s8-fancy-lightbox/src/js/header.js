/*!
 * JS from the Sideways8 Fancy Lightbox plugin.
 * @author Sideways8 Interactive, LLC http://sideways8.com/
 * @package S8_Fancy_Lightbox
 */
(function ( $, win, doc, undefined ) {
    var S8FancyLightbox = win.S8FancyLightbox || {};