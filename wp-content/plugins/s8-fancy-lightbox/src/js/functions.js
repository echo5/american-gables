S8FancyLightbox.fn = {

    ajaxLightbox: function() {
        // TODO: Look at adding ajax lightbox functionality!
    },

    formLightbox: function() {
        $('.s8fl-inline-form').magnificPopup({
            type: 'inline',
            removalDelay: ( S8FancyLightbox.enableFade ? 300 : 0 ),
            mainClass: ( S8FancyLightbox.enableFade ? 's8fl-fade ' : '' ) + 's8fl-inline-wrap',
            midClick: true,
            focus: 'input',
            fixedContentPos: false
        });
    },

    image: function() {
        $('.s8fl-image').each(function() {
            $(this).magnificPopup({
                type: 'image',
                removalDelay: ( S8FancyLightbox.enableFade ? 300 : 0 ),
                mainClass: ( S8FancyLightbox.enableFade ? 's8fl-fade ' : '' ) + 's8fl-image-wrap',
                fixedContentPos: false
            });
        });
    },

    imageGallery: function() {
        $('.s8fl-image-gallery').each(function() {
            $(this).magnificPopup({
                type: 'image',
                removalDelay: ( S8FancyLightbox.enableFade ? 300 : 0 ),
                mainClass: ( S8FancyLightbox.enableFade ? 's8fl-fade ' : '' ) + 's8fl-gallery-wrap',
                delegate: 'a',
                preload: [1,2], // Pre-load 1 previous image, next 2 images
                gallery: {
                    enabled: true
                },
                fixedContentPos: false
            });
        });
    },

    inlineLightbox: function() {
        $('.s8fl-inline').magnificPopup({
            type: 'inline',
            removalDelay: ( S8FancyLightbox.enableFade ? 300 : 0 ),
            mainClass: ( S8FancyLightbox.enableFade ? 's8fl-fade ' : '' ) + 's8fl-inline-wrap',
            midClick: true,
            fixedContentPos: false
        });
    },

    // Handle maps lightbox
    mapsLightbox: function() {
        $('.s8fl-map').each(function() {
            $(this).magnificPopup({
                type: 'iframe',
                removalDelay: ( S8FancyLightbox.enableFade ? 300 : 0 ),
                mainClass: ( S8FancyLightbox.enableFade ? 's8fl-fade ' : '' ) + 's8fl-map-wrap',
                fixedContentPos: false
            });
        });
    },

    // Handle video lightbox
    videoLightbox: function() {
        $('.s8fl-video').each(function() {
            $(this).magnificPopup({
                type: 'iframe',
                removalDelay: ( S8FancyLightbox.enableFade ? 300 : 0 ),
                mainClass: ( S8FancyLightbox.enableFade ? 's8fl-fade ' : '' ) + 's8fl-video-wrap',
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com',
                            id: 'v=',
                            src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0'
                        }
                    }
                },
                fixedContentPos: false
            });
        });
    },

    // Handle lightbox functionality for WordPress galleries
    wpGallery: function() {
        $('.gallery').each(function() {
            // Check for attachment page links!
            var link = $(this).find('a').first().attr('href');
            if ( -1 == link.indexOf('attachment_id=') ) {
                // Gallery appears to not be using attachment pages, proceed with lightbox!
                $(this).magnificPopup({
                    type: 'image',
                    removalDelay: ( S8FancyLightbox.enableFade ? 300 : 0 ),
                    mainClass: ( S8FancyLightbox.enableFade ? 's8fl-fade ' : '' ) + 's8fl-wp-gallery-wrap',
                    delegate: 'a',
                    preload: [1,2], // Pre-load 1 previous image, next 2 images
                    gallery: {
                        enabled: true
                    }
                });
            }
        });
    }

};