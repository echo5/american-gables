$(doc).ready( function() {

    if ( S8FancyLightbox.enableWPGalleries ) {
        S8FancyLightbox.fn.wpGallery();
    }

    S8FancyLightbox.fn.formLightbox();

    S8FancyLightbox.fn.image();

    S8FancyLightbox.fn.imageGallery();

    S8FancyLightbox.fn.inlineLightbox();

    S8FancyLightbox.fn.mapsLightbox();

    S8FancyLightbox.fn.videoLightbox();

} );
