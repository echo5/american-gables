// https://github.com/gruntjs/grunt-contrib-cssmin
module.exports = {
    plugin: {
        files: {
            '<%= paths.dest.min.css %>': ['<%= paths.dest.scss %>']
        }
    }
};
