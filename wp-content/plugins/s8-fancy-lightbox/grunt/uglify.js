// https://github.com/gruntjs/grunt-contrib-uglify
module.exports = {
    js: {
        options: {
            mangle: true,
            compress: true,
            preserveComments: 'some'
        },
        files: {
            '<%= paths.dest.min.js %>': ['<%= paths.dest.js %>']
        }
    }
};
