// https://github.com/gruntjs/grunt-contrib-sass
module.exports = {
    plugin: {
        options: {
            style: 'expanded',
            lineNumbers: true
        },
        src: [
            '<%= paths.scss %>'
        ],
        dest: '<%= paths.dest.scss %>'
    }
};
