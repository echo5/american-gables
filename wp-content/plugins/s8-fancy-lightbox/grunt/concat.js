// https://github.com/gruntjs/grunt-contrib-concat
module.exports = {
    js: {
        src: [
            '<%= paths.js %>'
        ],
        dest: '<%= paths.dest.js %>'
    }
};
