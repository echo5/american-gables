<?php
/**
 * Plugin Name: Sideways8 Fancy Lightbox
 * Plugin URI: http://sideways8.com/plugins/
 * Description: A lightbox plugin built on top of Magnific Popup that provides ease of use both for developers and end-users.
 * Tags: s8, sideways8, sideways 8, magnific popup, lightbox
 * Version: 2.0.0
 * Author: Sideways8 Interactive, LLC
 * Author URI: http://sideways8.com/
 * License: GPLv3
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * Class S8_Fancy_Lightbox
 * @package S8_Fancy_Lightbox
 */
class S8_Fancy_Lightbox {
	/**
	 * Version of the lightbox plugin, should follow semantic versioning! See http://semver.org/
	 *
	 * @var string
	 */
	protected $version = '2.0.0';

	/**
	 * Stores our lightbox callbacks and arguments until wp_footer, where we will output them.
	 *
	 * @var array
	 */
	protected $lightbox = array();

	/**
	 * Default lightbox args
	 *
	 * @var array
	 */
	protected $lightbox_args = array(
		'callback' => null,
		'class'    => '', // can be array or space separated list of CSS classes
		'file'     => null,
		/*'is_home'     => false,
		'is_front_page' => false,
		'is_page'       => false,
		'is_single'     => false,*/ // TODO: Need to think this through!
	);

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, '_enqueue_scripts' ) );
		add_action( 's8fl_add_lightbox', array( $this, '_add_lightbox' ), 10, 2 );
		add_action( 'wp_footer', array( $this, '_output_lightbox' ) );
	}

	/**
	 * Enqueues our styles and scripts on the frontend
	 *
	 * @internal
	 * @since 0.8.0
	 */
	public function _enqueue_scripts() {
		if ( ! is_admin() ) {
			$min = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
			wp_enqueue_style( 's8fl', plugins_url( "css/s8fl{$min}.css", __FILE__ ), array(), $this->version );
			wp_enqueue_script( 's8fl', plugins_url( "js/s8fl{$min}.js", __FILE__ ), array( 'jquery' ), $this->version, true );
			wp_localize_script( 's8fl', 'S8FancyLightbox', array(
				'enableFade'        => true, // TODO: Make admin setting for this!
				'enableWPGalleries' => true, // TODO: Make admin setting for this!
			) );
		}
	}

	/**
	 * Adds a lightbox to be output on footer. This is called via `do_action('s8fl_add_lightbox', $id, $callback, $args)`
	 *
	 * @internal
	 * @since 2.0.0
	 *
	 * @param string $id
	 * @param array $args
	 *
	 * @return bool True on success, false otherwise. Will output error to error log if WP_DEBUG is turned on.
	 */
	public function _add_lightbox( $id, $args = array() ) {
		// All lightbox are guilty until proven innocent
		$has_error = true;

		// Check for obvious errors
		if ( empty( $id ) ) {
			$this->_log_error( 'You must supply a lightbox ID!' );
		} elseif ( isset( $this->lightbox[ $id ] ) ) {
			$this->_log_error( "A lightbox with the ID '{$id}' already exists. ID must be unique!" );
		} else {
			$args = wp_parse_args( $args, $this->lightbox_args );
			if ( empty( $args['callback'] ) && empty( $args['file'] ) ) {
				$this->_log_error( "No output was supplied for the lightbox with the ID: '{$id}'! Output must be supplied!" );
			} elseif ( is_null( $args['file'] ) && ! is_callable( $args['callback'] ) ) {
				$this->_log_error( "Invalid callback for lightbox with ID: '{$id}'! When using 'callback' a valid, callable function must be supplied!" );
			} elseif ( is_null( $args['callback'] ) && ! file_exists( $args['file'] ) ) {
				$this->_log_error( "Invalid file supplied for lightbox with ID: '{$id}'! When using 'file', the full server path to an existing file must be supplied!" );
			} else {
				// Nothing obvious, let's say we don't have any issues!
				$has_error = false;
			}
		}

		if ( ! $has_error ) {
			$this->lightbox[ $id ] = $args;
		}

		return ! $has_error;
	}

	/**
	 * Outputs all add lightbox on the `wp_footer` action hook.
	 *
	 * @internal
	 * @since 2.0.0
	 */
	public function _output_lightbox() {
		if ( ! empty( $this->lightbox ) ) {
			/**
			 * Filter all registered lightbox before output
			 */
			$this->lightbox = apply_filters( 's8fl_filter_lightbox', $this->lightbox );

			// We actually have lightboxen to output
			foreach ( $this->lightbox as $id => $args ) {
				/**
				 * Allows filtering the lightbox arguments before output based on the lightbox ID
				 */
				$args = apply_filters( "s8fl_lightbox_args_{$id}", $args );

				$id = esc_attr( $id );

				if ( ! is_array( $args['class'] ) ) {
					if ( ! empty( $args['class'] ) ) {
						$args['class'] = preg_split( '#\s+#', $args['class'] );
					} else {
						$args['class'] = array();
					}
				}

				$classes = array_merge( array(
					'mfp-hide',
					's8fl-inline-style',
				), $args['class'] );
				$classes = esc_attr( implode( ' ', $classes ) );

				// TODO: Add ability to conditionally output lightbox? Use args, filter, or both?
				echo "<div id='s8fl-{$id}' class='{$classes}'>";
				if ( ! is_null( $args['callback'] ) && is_callable( $args['callback'] ) ) {
					call_user_func( $args['callback'] );
				} elseif ( ! is_null( $args['file'] ) && file_exists( $args['file'] ) ) {
					require( $args['file'] );
				} else {
					$this->_log_error( "Oops! Looks like we are missing output for the lightbox with ID '{$id}'." );
				}
				echo "</div>";
			}
		}
	}

	/**
	 * Used to output an error message if WP_DEBUG is turned on, otherwise does nothing. YAY for reduced output!
	 *
	 * @internal
	 * @since 2.0.0
	 *
	 * @param string $error_message
	 */
	protected function _log_error( $error_message ) {
		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			error_log( 'S8FL ERROR: ' . $error_message );
		}
	}

}

new S8_Fancy_Lightbox();
